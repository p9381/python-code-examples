# Math operations
# Addition              : 3 + 2
# Subtraction           : 3 - 2
# Multiplication        : 3 * 2
# Division              : 3 / 2
# Floor Division        : 3 // 2
# Exponent              : 3 ** 2
# Modulus               : 3 % 2

num1 = 3
num2 = 4

# rounding

print(round(num1 / num2, 1))

num1 = num1 + 1
print(num1)

num1 *= 2
print(num1)

# Comparisons
# Equal                 : 3 == 2
# Not Equal             : 3 != 2
# Greater Than          : 3 > 2
# Less Than             : 3 < 2
# Greater or Equal      : 3 <= 2
# Less or Equal         : 3 >= 2

print(num1 <= num2)
