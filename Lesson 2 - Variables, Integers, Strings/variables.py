message = "hello world!"
print(message)

my_message = "hello world" # correct!
MyMessage = "hello world" # wrong way!
print(MyMessage)

msg = "hello world"

msg = "this won't work"
print(msg)

msg = "this doesn't work"
print(msg)

msg = 'this "Will not" work'
print(msg)

msg = "this 'will' not work"
print(msg)
