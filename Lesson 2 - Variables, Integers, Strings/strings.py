# link no - lane no - start - end
lane_segment = "157-2-0-1200"

# Evaluating lane: lane number on link: link number ...

lane_segment.split("-")
print(lane_segment.split("-"))

link_no = lane_segment.split("-")[0]
lane_no = lane_segment.split("-")[1]

print("Evaluating lane: " + lane_no + " on link: " + link_no + "...")

eval_seg_txt = f"Lane number {lane_no} on link {link_no} is LOS C"
print(eval_seg_txt)

eval_seg_txt_old = "Lane number {} on link {} is LOS {}".format(lane_no, link_no, "C")
print(eval_seg_txt_old)

title_block = "sign and pavement marking plan"
title_block = title_block.upper()
print(title_block)
