# Solutions

This folder contains solutions to example problems.

You can click on each solution and view the problem solution. There are typically
many solutions to a given problem. If you found an alternative solution that
might be an improvement on the ones provided, let me know and I can review and
update the solution file.
