"""Given a list of random values, return a shorter list of only unique values.

for example:
    random_list = [1, 4, 523, 24, 1, 4, 55, 45, 34, 28, 29, 28, 77, 1, 45, 56]

return a list of only the unique values:
    unique_list = [1, 34, 4, 523, 45, 77, 55, 24, 56, 28, 29]
"""
random_list = [1, 4, 523, 24, 1, 4, 55, 45, 34, 28, 29, 28, 77, 1, 45, 56]

unique_list = list(set(random_list))
