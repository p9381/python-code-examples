"""You're given two lists and need to find all the common items between both lists.

for example there are two traffic count stations in close proximity and you'd
like to compare traffic volumes between stations. But, the stations each only counted
seven days of data but not consecutively.

Below are the count dates for each station.
    count_station_a = ["1/5/2021", "1/8/2021", "1/11/2021", "1/12/2021", "1/15/2021", "1/17/2021", "1/19/2021"]
    count_station_b = ["1/5/2021", "1/6/2021", "1/8/2021", "1/11/2021", "1/15/2021", "1/16/2021", "1/17/2021"]

The common count dates between each station should be:
    common = ['1/5/2021', '1/8/2021', '1/11/2021', '1/15/2021', '1/17/2021']
"""

count_station_a = [
    "1/5/2021",
    "1/8/2021",
    "1/11/2021",
    "1/12/2021",
    "1/15/2021",
    "1/17/2021",
    "1/19/2021",
]
count_station_b = [
    "1/5/2021",
    "1/6/2021",
    "1/8/2021",
    "1/11/2021",
    "1/15/2021",
    "1/16/2021",
    "1/17/2021",
]

# using set.intersection built in function (best):
common = set(count_station_a).intersection(count_station_b)

# using list comprehension (medium):
common = [c_date for c_date in count_station_a if c_date in count_station_b]

# using a for loop (slowest):
common = []
for c_date in count_station_a:
    if c_date in count_station_b:
        common.append(c_date)
