"""Create a function which takes a list of lists and flattens it into a one dimensional list.
The function should take a single parameter and return a list.

For example, passing the nested list of:
[[1, 2], [3,4]]

should return:
[1, 2, 3, 4]
"""


def flatten_short(nested_list: list[list[int]]) -> list:
    """Takes a nested list and flattens it."""
    return [item for inner_list in nested_list for item in inner_list]


def flatten_long(nested_list: list[list[int]]) -> list:
    """Takes a nested list and flattens it."""
    flat_list = []
    for inner_list in nested_list:
        flat_list.extend([item for item in inner_list])
    return flat_list
