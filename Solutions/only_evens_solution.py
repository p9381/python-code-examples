"""Write a function called "only_evens" which takes a start value and end value
and returns a list of all even numbers between the two values (including start
and end values).

For example, for a start of 1 and end of 10 the function should return:
    [2, 4, 6, 8, 10]
"""


def only_evens(start: int, end: int) -> list[int]:
    """Returns a list of even numbers between start and end values."""
    return [even for even in range(start, end + 1) if even % 2 == 0]
