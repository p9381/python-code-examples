device_names = [
    "95-N-N-13.4-CCTV-AX-DMAC",
    "95-N-S-13.4-CCTV-AX-DMAC",
    "95-N-13.4-MVDS-WA-T",
    "95-N-14.4-ICAB-T",
    "95-N-14.4-FFB-TA-T",
    "EXIST. 95-M-S-14.6-CCTV-AX-DMAC",
    "95-S-14.6-ICAB-T",
    "EXIST. 95-M-N-14.6-CCTV-AX-DMAC",
    "95-S-14.6-FFB-TA-T",
]

cctv_list = []
for device in device_names:
    if "CCTV" in device:
        cctv_list.append(device)

print(cctv_list)

cctv_list = [device.lower() for device in device_names if "CCTV" in device]

print(cctv_list)
