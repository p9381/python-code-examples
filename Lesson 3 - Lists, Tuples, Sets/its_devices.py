my_list = [1, 2, 3, 4]

device_names = [
    "95-N-N-13.4-CCTV-AX-DMAC",
    "95-N-S-13.4-CCTV-AX-DMAC",
    "95-N-13.4-MVDS-WA-T",
    "95-N-14.4-ICAB-T",
    "95-N-14.4-FFB-TA-T",
    "EXIST. 95-M-S-14.6-CCTV-AX-DMAC",
    "95-S-14.6-ICAB-T",
    "EXIST. 95-M-N-14.6-CCTV-AX-DMAC",
    "95-S-14.6-FFB-TA-T",
]

# access item in list
single_device = device_names[1]
# print(single_device)

# last item in list
last_item = device_names[-1]
# print(last_item)

# print(device_names[0:3])

# add a device
new_device = "95-N-14.7-ICAB-T"

device_names.append(new_device)
# print(device_names)

# remove a device
removed_device = device_names.pop(4)
# print(removed_device)
# print(device_names)

# change device name
wrong_device = "95-S-14.6-ICAB-T"
correct_device = "95-S-14.7-ICAB-T"

device_names.remove(wrong_device)
device_names.insert(5, correct_device)
# print(device_names)

msg = f"""
Dear Mr./Mrs. Contractor,

We have removed {removed_device} from the device list and we have added 
{new_device} to the list. Additionally, we have re-named {wrong_device} to
{correct_device}. The complete list of devices is now: \n
"""

display_device_names = "\n".join(device_names)

msg = f"{msg}{display_device_names}"

print(msg)
