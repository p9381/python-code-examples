from data import ny_hourly_volumes

# start_day_num = int(input("Starting day number: "))
# end_day_num = int(input("Ending day number: "))
# hour = int(input("Hour of day (24hr format): "))

# start_slice = (start_day_num-1) * 24 + hour
# end_slice = (end_day_num) * 24 + hour

# print(ny_hourly_volumes[start_slice:end_slice:24])

# compute the AADT

total_volume = sum(ny_hourly_volumes)
total_days = len(ny_hourly_volumes) / 24

aadt = total_volume / total_days

print(f"The un-rounded Average Annual Daily Traffic (AADT) is: {aadt} vpd")
print(f"The rounded Average Annual Daily Traffic (AADT) is: {aadt:,.0f} vpd")

# compute the DHV

ny_hourly_volumes.sort(reverse=True)

hour = 30

print(f"The Design Hourly Volume is: {ny_hourly_volumes[hour-1]} vph")

# compute k_factor

dhv = ny_hourly_volumes[hour-1]

k_factor = dhv / aadt

print(f"The k-factor is: {k_factor:.3f}")
