device_names = {
    "95-N-N-13.4-CCTV-AX-DMAC",
    "95-N-S-13.4-CCTV-AX-DMAC",
    "95-N-13.4-MVDS-WA-T",
    "95-N-14.4-ICAB-T",
    "95-N-14.4-FFB-TA-T",
    "EXIST. 95-M-S-14.6-CCTV-AX-DMAC",
    "95-S-14.6-ICAB-T",
    "EXIST. 95-M-N-14.6-CCTV-AX-DMAC",
    "95-S-14.6-FFB-TA-T",
    "NEW DEVICE"
}

# try to change a value
# device_names[0] = "NEW DEVICE"
device_names.add("NEW DEVICE")

print(device_names)
