import json
from pathlib import Path

config_file_location = Path(__file__).parent / "config.json" # may be different for each persons vs code setup

config_file = open(config_file_location, encoding="UTF-8")

config = json.load(config_file)

print(config)
