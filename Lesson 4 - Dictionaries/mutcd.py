sign = {
    "name": "Stop",
    "id": "R1-1",
    "type": "Regulatory",
    "height": 36,
    "width": 36,
}
# alt_sign = dict(name="Stop", id="R1-1", type="Regulatory", height=36, width=36)
# print(sign)
# print(alt_sign)

# acess a key value
# print(sign["type"])

# update key values
sign["height"] = 48
sign["width"] = 48
# print(sign)

# color = sign["color"] # KeyError!
area = sign.get("area")
# print(area)

area = sign["height"] * sign["width"] / 144

sign["area"] = area

# print(f"The sign area is: {sign['area']} SF")

sign.update(
    {
        "figure_url": "https://mutcd.fhwa.dot.gov/htm/2009/part2/fig2b_01_longdesc.htm",
        "section_url": "https://mutcd.fhwa.dot.gov/htm/2009/part2/part2b.htm#section2B05",
    }
)
# print(sign)

# {
#     sign_1_id: {
#         "name": "...",
#         "id": "...",
#         "type": "...",
#         "height": XX,
#         "width": YY,
#         "area": Z.ZZZ,
#         "figure_url": "...",
#         "section_url": "...",
#     },
#     sign_2_id: {
#         "name": "...",
#         "id": "...",
#         "type": "...",
#         "height": XX,
#         "width": YY,
#         "area": Z.ZZZ,
#         "figure_url": "...",
#         "section_url": "...",
#     },
# }

# re-format the dictionary so we can add more signs
signs = {sign["id"]: sign}
print(signs)

new_sign = {
    "R1-2": {
        "name": "Yield",
        "id": "R1-2",
        "type": "Regulatory",
        "height": 36,
        "width": 36,
        "area": 3.8971,
        "figure_url": "https://mutcd.fhwa.dot.gov/htm/2009/part2/fig2b_01_longdesc.htm",
        "section_url": "https://mutcd.fhwa.dot.gov/htm/2009/part2/part2b.htm#section2B08",
    }
}

signs.update(new_sign)

sign_id = input("What sign do you want search for? (enter MUTCD sign id) ")
sign = signs[sign_id]

print(f"Here is the figure URL for {sign_id}: {sign['figure_url']}")
