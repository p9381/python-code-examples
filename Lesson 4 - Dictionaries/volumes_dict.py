from data import ny_hourly_volumes, date_times

# print(f"Volumes: {ny_hourly_volumes[0:5]}")
# print(f"Dates: {date_times[0:5]}")

vol_dict = dict(zip(date_times, ny_hourly_volumes))

# print(vol_dict["12/31/2017 23:00"])

date = input("Enter day of the year (mm/dd/yyyy): ")
hour = input("Enter hour of the day (0-23): ")

date_search = f"{date} {hour}:00"

print(f"The volume on {date} at {hour}:00 was {vol_dict[date_search]} vph")
