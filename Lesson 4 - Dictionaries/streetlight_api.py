import datetime
import json
import requests # must install the requests python package to run

now = datetime.datetime.utcnow().isoformat()

STL_KEY = 'abcdefghijklmnop' # You need to pay StreetLight for a package with an API key and paste it here
INSIGHT_LOGIN_EMAIL = 'edit.this@example.com'

ZONE_SET_NAME = "Two_Neighborhoods_{}".format(now)
ANALYSIS_NAME = "OD_for_TN_{}".format(now)

CREATE_ANALYSIS_REQUEST = {
    "insight_login_email": INSIGHT_LOGIN_EMAIL,
    "analysis_name": ANALYSIS_NAME,
    "analysis_type": "OD_Analysis",
    "travel_mode_type": "All_Vehicles",
    "description": "",
    "oz_sets": [{"name":ZONE_SET_NAME}],
    "dz_sets": [{"name":ZONE_SET_NAME}],
    "data_periods": "1-2018, 2-2018, 3-2018",
    "day_types": "All Days|17, Weekday|14, Weekend Day|67", # 1 = Monday, ... # 7 = Sunday
    "day_parts": "All Day|0023, Early AM|0005, Peak AM|0609, Mid-Day|1014, Peak PM|1518, Late PM|1923", # XXYY. XX = Start Hour, YY = End Hour
    "trip_attributes": False,
    "traveler_attributes": False
}

resp = requests.post(
    'https://insight.streetlightdata.com:443/api/v2/analyses',
    headers = {'content-type': 'application/json', 'x-stl-key': STL_KEY},
    data = json.dumps(CREATE_ANALYSIS_REQUEST))
