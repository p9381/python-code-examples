# Introduction to Python Course

This course focuses on providing you with an understanding of the basics of Python programming. At the moment, creating and running your own Python scripts and software may seem daunting to you. During this course, you'll learn how to write Python scripts that can help solve some seemingly complex problems. You'll be able to process and analyze data efficiently. When you're presented a challenging problem, you'll have the tools to recognize that there may be a solution with Python.
Overall, the course is organized into parts.

In the first part, I will cover all the basics in Python: installing Python, variables, math operations, conditional logic, for loops, lists, dictionaries, and functions. These are the basic concepts in Python that are needed to effectively write your own Python scripts.

The second part will cover data processing and visualization. Python is increasingly used in the world of data science and this section you will learn the tools available to process and visualize data. I will use data from a variety of sources and demonstrate how to create charts and tables. It will also cover how you will want to structure your data so that it may be used conveniently by other programs.

The last part will cover specifically requested topics such as integration with Vissim and Visum. Additional topics may be included as the course progresses.

To become proficient with Python programming, this course requires your active participation. There is no magic pill to learn Python. If you want results, you need to apply the things I teach to your own work and gain experience with them. To help with this, I'll use lots of examples, all relevant to the field of transportation. My hope is that the examples I provide are going to be relevant things that you can apply immediately. I will also provide several quizzes to test your knowledge.

If you want to audit this course to understand the capabilities of Python, then practicing Python and taking quizzes will be optional for you. However, my expected outcome for those auditing the course is that you will be able to recognize problems that are solvable in Python.

I hope you're excited to get started!
