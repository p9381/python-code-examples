veh = {
    "id": 2755,
    "x": 5174.841,
    "y": 1217.259,
    "in_queue": True,
    "stopped_ts": 5400.0,
    "stopped_dur": 3,
}

# for key in veh:
#     print(key)


# for val in veh.values():
#     print(val)


for key, val in veh.items():
    print(f"{key} -> {val}")
