
def device_info(*args, **kwargs):
    print(args)
    print(kwargs)


# device_info(5, 6, 40, name="CCTV-23", direction="WB", id="23")

dimensions = (5, 6, 40)
info = {"name": "CCTV-23", "direction": "WB"}

device_info(*dimensions, **info)
