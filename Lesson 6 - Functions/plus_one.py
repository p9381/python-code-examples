def plus_one(x):
    return x + 1

y = plus_one(8)

print(y)


def plus_seven_to_power(x, p):
    return (x + 7) ** p


z = plus_seven_to_power(p=5, x=3)

print(z)
