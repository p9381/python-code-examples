# Signalized LOS HCM 6 Scale

# LOS   Delay (sec/veh)
# A     <= 10
# B     10 - 20
# C     20 - 35
# D     35 - 55
# E     55 - 80
# F     > 80

# LOS F also assigned if v/c >= 1.0


# barebones function
def barebones_calc_los(delay, vc):
    if delay <= 10:
        los = "A"
    # elif DELAY > 10 and DELAY <= 20:
    elif 10 < delay <= 20:
        los = "B"
    elif 20 < delay <= 35:
        los = "C"
    elif 35 < delay <= 55:
        los = "D"
    elif 55 < delay <= 80:
        los = "E"
    else:
        los = "F"

    if vc >= 1:
        los = "F"
    return los


# docstring function
def docstring_calc_los(delay, vc):
    """Calculates los based on HCM 6 signalized los scale."""
    if delay <= 10:
        los = "A"
    elif 10 < delay <= 20:
        los = "B"
    elif 20 < delay <= 35:
        los = "C"
    elif 35 < delay <= 55:
        los = "D"
    elif 55 < delay <= 80:
        los = "E"
    else:
        los = "F"

    if vc >= 1:
        los = "F"
    return los


# type hints function
# note before python 3.10 use:
# from typing import Union
# def proper_signalized_los(delay: Union[int, float], vc: float):
def typehints_calc_los(delay: int | float, vc: float):
    """Calculates los based on HCM 6 signalized los scale."""
    if delay <= 10:
        los = "A"
    elif 10 < delay <= 20:
        los = "B"
    elif 20 < delay <= 35:
        los = "C"
    elif 35 < delay <= 55:
        los = "D"
    elif 55 < delay <= 80:
        los = "E"
    else:
        los = "F"

    if vc >= 1:
        los = "F"
    return los

def determine_los(delay: int | float, vc: float) -> str:
    """Calculates los based on HCM 6 signalized los scale."""
    if delay <= 10:
        los = "A"
    elif 10 < delay <= 20:
        los = "B"
    elif 20 < delay <= 35:
        los = "C"
    elif 35 < delay <= 55:
        los = "D"
    elif 55 < delay <= 80:
        los = "E"
    else:
        los = "F"

    if vc >= 1:
        los = "F"
    return los


results = [(15, 0.25), (36, 0.89), (75, 1.02), (55, 0.99)]
for int_delay, int_vc in results:
    sig_los = determine_los(delay=int_delay, vc=int_vc)
    print(sig_los)
