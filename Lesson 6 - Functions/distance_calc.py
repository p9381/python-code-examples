from math import sqrt


def calc_distance(point_1, point_2):
    return sqrt((point_1[0] - point_2[0]) ** 2 + (point_1[1] - point_2[1]) ** 2)


p1 = (75, 90)
p2 = (100, 114)

point_distance = calc_distance(p1, p2)
print(point_distance)


def calc_distance2(point_1, point_2=(0, 0)):
    return sqrt((point_1[0] - point_2[0]) ** 2 + (point_1[1] - point_2[1]) ** 2)


point_distance = calc_distance2(p1, p2)
print(point_distance)
