"""You're responding to an RFP and would like to know how frequently an RFP uses
a certain word or phrase. That way you can try to figure out what aspects of the
project are most important to the client.

Write a function which takes any string of text and a search term and returns 
the number of times that search term occurs in the text.

For example:
    rfp_text = "The Design-Builder will be required to submit a temporary traffic control plan (TTCP) and a Real Time 
                Traffic Management (RTTM) System for Work Zones consistent with their final design for MassDOT 
                approval. Preliminary traffic management and construction staging plans are provided in the BTC Plans. 
                The BTC Plans depict the intended staging and lane requirements during construction and are conceptual 
                in nature. The Design-Builder shall provide traffic control plans consistent with project delivery in their 
                proposal, and advance the traffic control plans to final design in accordance with the latest edition of the  
                MUTCD and MassDOT Chapter 6 Edition.  Alternative approaches proposed for the construction of the 
                bridge may result in a modified approach to TTCP's (from the BTC) during the different construction 
                phases.  The Design-Builder shall respond to District and Boston Traffic review comments in developing 
                the final traffic control plans. These requirements include, at a minimum, the following:"
    search_txt = "traffic control"

    The function should return 4.
"""
rfp_text = """The Design-Builder will be required to submit a temporary traffic control plan (TTCP) and a Real Time 
            Traffic Management (RTTM) System for Work Zones consistent with their final design for MassDOT 
            approval. Preliminary traffic management and construction staging plans are provided in the BTC Plans. 
            The BTC Plans depict the intended staging and lane requirements during construction and are conceptual 
            in nature. The Design-Builder shall provide traffic control plans consistent with project delivery in their 
            proposal, and advance the traffic control plans to final design in accordance with the latest edition of the  
            MUTCD and MassDOT Chapter 6 Edition.  Alternative approaches proposed for the construction of the 
            bridge may result in a modified approach to TTCP's (from the BTC) during the different construction 
            phases.  The Design-Builder shall respond to District and Boston Traffic review comments in developing 
            the final traffic control plans. These requirements include, at a minimum, the following:"""
search_txt = "traffic control"
