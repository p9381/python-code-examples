"""Given a list of vehicle traffic data:
vehicles = ["12315", 55, True, "red"]

and associated properties names for each list item:
properties = ["id", "speed", "is_moving", "color"]

create a dictionary called veh_data that makes each item in the list correspond
to the correct vehicle property. For example the end value should look like:

veh_data = {"id": "12315", "speed": 55, "is_moving": True, "color": "red"}
"""

vehicles = ["12315", 55, True, "red"]
properties = ["id", "speed", "is_moving", "color"]
