"""Given a dictionary of ITS devices online/offline status,
count the number of devices offline.

For example, consider the following dictionary:
device_statuses = {
    "ME-95-SB-CCTV": "online",
    "ME-95-NB-CCTV": "offline",
    "NH-93-NB-CCTV": "online",
}
in this case the number of offline devices is 1.

Write a function named num_offline that takes one parameter. The parameter is
a dictionary that maps device name strings to the string "online" or "offline",
as seen above.

Your function should return the number of devices offline.
"""
