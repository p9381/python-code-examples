# Challenges

This folder contains example problems for you to practice Python on your own. 

You can click on each example and read the problem statement. You can then copy
text into VS Code and begin trying out your solutions. Once you think you've 
found a solution you can look at the solution found in the Solutions folder.
