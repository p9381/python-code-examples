"""Create a function which takes a list of lists and flattens it into a one dimensional list.
The function should take a single parameter and return a list.

For example, passing the nested list of:
[[1, 2], [3,4]]

should return:
[1, 2, 3, 4]
"""
