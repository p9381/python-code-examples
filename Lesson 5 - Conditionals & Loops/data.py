import csv
from pathlib import Path

filename = Path(__file__).parent / "vissim_data.csv"

with open(filename, newline="", encoding="utf-8") as csvfile:

    vissim_data = [
        {key: val for key, val in row.items()}
        for row in csv.DictReader(csvfile, skipinitialspace=True)
    ]

    for row in vissim_data:
        row["simsec"] = float(row["simsec"])
        row["link"] = int(row["link"])
        row["x"] = float(row["x"])
        row["y"] = float(row["y"])
        if row["in_queue"] == "False":
            row["in_queue"] = False
        else:
            row["in_queue"] = True
