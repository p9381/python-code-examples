import random

magic_number = random.randint(1, 10)
guess = True

while guess:
    guess = int(input("guess a number between 1 and 10: "))
    if guess < magic_number:
        print("Too low, try again!")

    # the live lesson had a bug where this was another "if" statement instead of "elif"
    elif guess > magic_number:
        print("Too high, try again!")
    else:
        print("You guessed it! You won!")
        break
