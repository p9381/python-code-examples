haystack = ["item", "another thing", "needle", "thing 4"]

for item in haystack:
    if item == "needle":
        # do something when we find our "needle"
        print("You found the needle")
        continue # continue to the next item in the list

    print("You didn't find the needle")
