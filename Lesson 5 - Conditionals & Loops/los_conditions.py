# Signalized LOS HCM 6 Scale

# LOS   Delay (sec/veh)
# A     <= 10
# B     10 - 20
# C     20 - 35
# D     35 - 55
# E     55 - 80
# F     > 80

# LOS F also assigned if v/c >= 1.0

DELAY = float(input("What is the delay (sec/veh)? "))
VC = float(input("What is the volume to capacity ratio? "))

if DELAY <= 10:
    los = "A"
# elif DELAY > 10 and DELAY <= 20:
elif 10 < DELAY <= 20:
    los = "B"
elif 20 < DELAY <= 35:
    los = "C"
elif 35 < DELAY <= 55:
    los = "D"
elif 55 < DELAY <= 80:
    los = "E"
else:
    los = "F"

if VC >= 1:
    los = "F"

print(los)
