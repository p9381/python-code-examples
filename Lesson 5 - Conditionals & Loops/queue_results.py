import csv

from data import vissim_data

MONITOR_LINKS = [37, 38, 10036]

# vehicles = {
#     "veh_id": {
#         "id": "123456",
#         "x": 10,
#         "y": 15,
#         "in_queue": True,
#         "link": 149,
#         "stopped_ts": 100,
#         "stopped_dur": 5,
#     }
# }

vehicles = {}

for row in vissim_data:

    # if the vehicle is not in our vehicles dictionary, add it.
    if vehicles.get(row["id"]) is None:
        veh = {
            "id": row["id"],
            "x": row["x"],
            "y": row["y"],
            "in_queue": row["in_queue"],
            "link": row["link"],
            "stopped_ts": 0,
            "stopped_dur": 0,
        }
        vehicles[row["id"]] = veh

    veh = vehicles.get(row["id"])

    # capture vehicles that are in a queue and in our monitored links, and don't have a stopped_ts yet.
    # if the vehicle in our csv file is in a queue AND in our monitored links AND vehicle doesn't have a stopped timestamp
    if row["in_queue"] and row["link"] in MONITOR_LINKS and veh["stopped_ts"] == 0:
        vehicles[row["id"]] = {
            "id": row["id"],
            "x": row["x"],
            "y": row["y"],
            "in_queue": row["in_queue"],
            "link": row["link"],
            "stopped_ts": row["simsec"],
            "stopped_dur": 0,
        }

    # Capture vehicles that were in a queue and are no longer in a queue then record how long it was in a queue
    # if the vehicle is not in a queue AND the stopped timestamp is > 0 AND it doesn't have a stopped duration
    if not row["in_queue"] and veh["stopped_ts"] > 0 and not veh["stopped_dur"]:
        vehicles[row["id"]].update({"stopped_dur": row["simsec"] - veh["stopped_ts"]})

# use list comprehension to filter out vehicles that don't have a stopped duration
stopped_vehicles = [val for _, val in vehicles.items() if val["stopped_dur"] > 0]

# column names for our output .csv file
columns = ["id", "x", "y", "link", "in_queue", "stopped_ts","stopped_dur"]

# save results to a .csv file
with open("stopped_vehicles.csv", "w", newline="") as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames = columns)
    writer.writeheader()
    writer.writerows(stopped_vehicles)
