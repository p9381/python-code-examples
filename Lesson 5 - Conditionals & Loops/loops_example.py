link_names = ["I-90 EB", "I-90 EB Exit 5", "I-90 WB Exit 5"]

# for link_name in link_names:
#     print(link_name)

# nested loops
for link_name in link_names:
    for num in [1, 2, 3]:
        print(f"{link_name} - {num}")
